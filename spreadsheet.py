import gspread
from oauth2client.service_account import ServiceAccountCredentials


# use creds to create a client to interact with the Google Drive API
class Gspread:
    def __init__(self):
        self.scope = ['https://spreadsheets.google.com/feeds',
                    'https://www.googleapis.com/auth/drive']
        self.creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', self.scope)
        self.client = gspread.authorize(self.creds)

    def insert_data(self, data):
        # Find a workbook by name and open the first sheet
        # Make sure you use the right name here.
        sheet = self.client.open("StrawFromU").sheet1

        # Extract and print all of the values
        current_data_idx = len(sheet.col_values(1))

        # [Nama, ID Line, No. HP, Alamat, Kota, Provinsi, Kode Pos, Pesanan, Quantity, Status]
        sheet.insert_row(
            [str(data['nama']), str(data['idLine']),
             str(data['nohp']), str(data['alamat']), 
             str(data['provinsi']), str(data['kota']), 
             str(data['kodepos']), str(data['paket_pondA']), 
             str(data['paket_pondB']), str(data['paket_lagoon']), 
             str(data['paket_ocean']), 'Menunggu Pembayaran'], current_data_idx+1)
        return "Success"
        # sheet.insert_row(['Audric Fabian', 'auauauau', '081208120812', 'jl hurahura', 'jakarta utara', 'jakarta', '14240', 'Ocean', '2', 'Menunggu Pembayaran'], current_data_idx+1)

# gspread = Gspread()
# gspread.insert_data("hehe")