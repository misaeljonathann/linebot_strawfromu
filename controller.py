from flask import Flask, request, abort, render_template, jsonify, request, Markup
from flask_cors import CORS
from spreadsheet import Gspread

import os
import json
import requests
import http.client

from decouple import config
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, 
    CarouselColumn, CarouselTemplate, TemplateSendMessage,
    PostbackAction, MessageAction, URIAction,
    PostbackEvent, URIImagemapAction, ImagemapSendMessage,
    BaseSize, MessageImagemapAction, ImagemapArea
)
from rajaongkir import RajaOngkirApi

#Initialize Variable
gspread = Gspread()
app = Flask(__name__)
CORS(app)


# Define Variable
# get LINE_CHANNEL_ACCESS_TOKEN from your environment variable
line_bot_api = LineBotApi(
    config("LINE_CHANNEL_ACCESS_TOKEN",
            default=os.environ.get('LINE_CHANNEL_ACCESS_TOKEN'))
)
# get LINE_CHANNEL_SECRET from your environment variable
handler = WebhookHandler(
    config("LINE_CHANNEL_SECRET",
            default=os.environ.get('LINE_CHANNEL_SECRET'))
)
# get RAJA_ONGKIR from your environment variable
rajaOngkirApi = RajaOngkirApi(api_key=
    config("RAJA_ONGKIR",
            default=os.environ.get('RAJA_ONGKIR'))
)

@app.route("/")
def main():
    return render_template("index.html")

@app.route("/form", methods=['POST'])
def show_province():
    list_of_province = rajaOngkirApi.provinces()
    response_html = ''
    for province in list_of_province:
        response_html+='<option value=' + province['province_id'] + '>' + province['province'] + '</option>'
    return response_html

@app.route("/form-kota", methods=['POST'])
def show_cities():
    value = str(request.form.get('value'))
    response_html = ''
    cities_by_province = rajaOngkirApi.cities_by_province(value)
    for city in cities_by_province:
        response_html+='<option value=' + city['city_id'] + '>' + city['city_name'] + '</option>'
    return response_html

@app.route('/handle-data', methods=['POST'])
def handle_data():
    data = request.form
    return gspread.insert_data(data)

@app.route('/get-cost', methods=['POST'])
def get_cost():
    idKota = request.form['value']
    conn = http.client.HTTPSConnection("api.rajaongkir.com")
    payload = "origin=155&destination="+str(idKota)+"&weight=1000&courier=jne"
    headers = {
        'key': os.environ.get('RAJA_ONGKIR'),
        'content-type': "application/x-www-form-urlencoded"
    }

    conn.request("POST", "/starter/cost", payload, headers)

    res = conn.getresponse()
    data = res.read()
    output = ""
    result = json.loads(data.decode("utf-8"))

    # for every courier list
    for courierList in result['rajaongkir']['results']:

        #for every type
        for priceList in courierList['costs']:

            #for every available price
            for price in priceList['cost']:
                # output+="hehe"
                # output+='<option value=' + str(priceList['service']) + '> Service ' + priceList['service'] + ' = ' + str(price['value']) + '<br>'
                # + " = " + str(price['value']) + ""

                output+='<input type="radio" name="jenis-pengiriman" '+'value='+str(price['value'])+'> Service ' + priceList['service']+' = ' + str(price['value']) + '<br>'
                # output = str(price['value']) + " "
    return output

    #{'rajaongkir': {'query': {'origin': '155', 'destination': '23', 'weight': 1000, 'courier': 'jne'}, 'status': {'code': 200, 'description': 'OK'}, 'origin_details': {'city_id': '155', 'province_id': '6', 'province': 'DKI Jakarta', 'type': 'Kota', 'city_name': 'Jakarta Utara', 'postal_code': '14140'}, 'destination_details': {'city_id': '23', 'province_id': '9', 'province': 'Jawa Barat', 'type': 'Kota', 'city_name': 'Bandung', 'postal_code': '40111'}, 'results': [{'code': 'jne', 'name': 'Jalur Nugraha Ekakurir (JNE)', 'costs': [{'service': 'OKE', 'description': 'Ongkos Kirim Ekonomis', 'cost': [{'value': 10000, 'etd': '2-3', 'note': ''}]}, {'service': 'REG', 'description': 'Layanan Reguler', 'cost': [{'value': 11000, 'etd': '1-2', 'note': ''}]}, {'service': 'YES', 'description': 'Yakin Esok Sampai', 'cost': [{'value': 24000, 'etd': '1-1', 'note': ''}]}]}]}}

def show_carousel(token):
    carousel_template_message = TemplateSendMessage(
        alt_text='Carousel template',
        template=CarouselTemplate(
            columns=[
                CarouselColumn(
                    thumbnail_image_url='https://s3-ap-southeast-1.amazonaws.com/prelo/images/resized/base/products/5b625c3f05ba73013d46ecf3/sedotan-stainless-steel-new-00faef61f1ea-a6G8Kn-1-1533172799629.jpg',
                    title='Paket 1',
                    text='description1',
                    actions=[
                        PostbackAction(
                            label='postback1',
                            text='postback text1',
                            data='action=buy&itemid=1'
                        ),
                        # MessageAction(
                        #     label='message1',
                        #     text='message text1'
                        # ),
                        # URIAction(
                        #     label='uri1',
                        #     uri='http://example.com/1'
                        # )
                    ]
                ),
                CarouselColumn(
                    thumbnail_image_url='https://s3-ap-southeast-1.amazonaws.com/prelo/images/resized/base/products/5b625c3f05ba73013d46ecf3/sedotan-stainless-steel-new-00faef61f1ea-a6G8Kn-1-1533172799629.jpg',
                    title='Paket 2',
                    text='description2',
                    actions=[
                        PostbackAction(
                            label='postback2',
                            text='postback text2',
                            data='action=buy&itemid=2'
                        ),
                        # MessageAction(
                        #     label='message2',
                        #     text='message text2'
                        # ),
                        # URIAction(
                        #     label='uri2',
                        #     uri='http://example.com/2'
                        # )
                    ]
                ),
                CarouselColumn(
                    thumbnail_image_url='https://s3-ap-southeast-1.amazonaws.com/prelo/images/resized/base/products/5b625c3f05ba73013d46ecf3/sedotan-stainless-steel-new-00faef61f1ea-a6G8Kn-1-1533172799629.jpg',
                    title='Paket 3',
                    text='description3',
                    actions=[
                        PostbackAction(
                            label='postback3',
                            text='postback text3',
                            data='action=buy&itemid=3'
                        ),
                        # MessageAction(
                        #     label='message2',
                        #     text='message text2'
                        # ),
                        # URIAction(
                        #     label='uri2',
                        #     uri='http://example.com/2'
                        # )
                    ]
                )
            ]
        )
    )
    line_bot_api.reply_message(token, carousel_template_message)


def show_image_map(token):
    imagemap_message = ImagemapSendMessage(
        base_url='https://i.postimg.cc/y63ZcS0P/straw-4warna.jpg',
        alt_text='this is an imagemap',
        base_size=BaseSize(height=1040, width=1040),
        actions=[
            #Black
            MessageImagemapAction(
                text='#black',
                area=ImagemapArea(
                    x=0, y=0, width=520, height=520
                )
            ),
            #Hologram
            MessageImagemapAction(
                text='#hologram',
                area=ImagemapArea(
                    x=520, y=0, width=520, height=520
                )
            ),
            #RoseGold
            MessageImagemapAction(
                text='#rosegold',
                area=ImagemapArea(
                    x=0, y=520, width=520, height=520
                )
            ),
            #Silver
            MessageImagemapAction(
                text='#silver',
                area=ImagemapArea(
                    x=520, y=520, width=520, height=520
                )
            ),
        ]
    )
    line_bot_api.reply_message(token, imagemap_message)

@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    if event.message.text.lower() == "provinsi":
        list_of_city = json.dumps(rajaOngkirApi.provinces()) 
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=list_of_city)
        )
            
    elif event.message.text.lower() == "harga":
        output = get_cost(501, 114, 2000, 'jne')
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text='Ongkir dari Bekasi ke Bandung: \n' + output)
        )
    elif event.message.text.lower() == "paket":
        show_carousel(event.reply_token)
        gspread.insert_data()

    elif event.message.text.lower() == "warna":
        show_image_map(event.reply_token)

    else:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=event.message.text))


# Handle Postback Event on Carousel
@handler.add(PostbackEvent)
def handle_postback(event):
    action, itemId = event.postback.data.split("&")
    
    # if event.postback.data == 'ping':
    #     line_bot_api.reply_message(
    #         event.reply_token, TextSendMessage(text='pong'))
    # elif event.postback.data == 'datetime_postback':
    #     line_bot_api.reply_message(
    #         event.reply_token, TextSendMessage(text=event.postback.params['datetime']))
    # elif event.postback.data == 'date_postback':
    #     line_bot_api.reply_message(
    #         event.reply_token, TextSendMessage(text=event.postback.params['date']))

if __name__ == "__main__":
    app.run()
