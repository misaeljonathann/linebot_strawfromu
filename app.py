from flask import Flask, request, abort, jsonify

import os
import json
import requests

from decouple import config
from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)
from rajaongkir import RajaOngkirApi

#get RAJA_ONGKIR from your environment variable
rajaOngkirApi = RajaOngkirApi(api_key="04edd8c6d7a4f038f95231ea1f984d86")
parameters = {
    "origin": 55,
    "destination": 23,
    "weight": 1000,
    "courier": 'jne',
}
list_of_city = rajaOngkirApi.cities()
print(list_of_city)

# response = requests.get("https://api.rajaongkir.com/starter/cost", params=parameters)
# cost = rajaOngkirApi.cost_between_city(55, 55, 1000, 'jne')
# cost = rajaOngkirApi.cost_between_city(501, 114, 2000, 'jne')
# print(response.json())


app = Flask(__name__)

# with app.app_context():
    # raja

# list_province = []
# for data in list_of_city:
#     elem = '<option value=' + data['province_id'] + '>' + data['province'] + '</option>'
#     print(elem)
#     list_province.append(elem)



# import http.client

# def get_cost(origin, destination, weight, courier):

#     conn = http.client.HTTPSConnection("api.rajaongkir.com")
#     payload = "origin="+str(origin)+"&destination="+str(destination)+"&weight="+str(weight)+"&courier="+str(courier)
#     headers = {
#         'key': "04edd8c6d7a4f038f95231ea1f984d86",
#         'content-type': "application/x-www-form-urlencoded"
#     }

#     conn.request("POST", "/starter/cost", payload, headers)

#     res = conn.getresponse()
#     data = res.read()

#     output = json.loads(data.decode("utf-8"))

#     # for every courier list
#     for courierList in output['rajaongkir']['results']:

#         #for every type
#         for priceList in courierList['costs']:

#             #for every available price
#             for price in priceList['cost']:
#                 print("Service " + priceList['service'] + " = " + str(price['value']))

# get_cost(501, 114, 2000, 'jne')